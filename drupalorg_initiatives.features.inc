<?php

/**
 * Implementation of hook_node_info().
 */
function drupalorg_initiatives_node_info() {
  $items = array(
    'initiative' => array(
      'name' => t('Initiative'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
    'initiative_issue_grouping' => array(
      'name' => t('Initiative issue grouping'),
      'module' => 'features',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function drupalorg_initiatives_views_api() {
  return array(
    'api' => '2',
  );
}
