<?php

/**
 * Implementation of hook_content_default_fields().
 */
function drupalorg_initiatives_content_default_fields() {
  $fields = array();

  // Exported field: field_issue_groupings
  $fields['initiative-field_issue_groupings'] = array(
    'field_name' => 'field_issue_groupings',
    'type_name' => 'initiative',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'initiative_issue_grouping' => 'initiative_issue_grouping',
      'book' => 0,
      'forum' => 0,
      'initiative' => 0,
      'project_issue' => 0,
      'page' => 0,
      'project_project' => 0,
      'project_release' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_issue_groupings][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Issue groupings',
      'weight' => '31',
      'description' => '',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_issues
  $fields['initiative_issue_grouping-field_issues'] = array(
    'field_name' => 'field_issues',
    'type_name' => 'initiative_issue_grouping',
    'display_settings' => array(
      'weight' => '32',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'project_issue' => 'project_issue',
      'book' => 0,
      'forum' => 0,
      'initiative' => 0,
      'initiative_issue_grouping' => 0,
      'page' => 0,
      'project_project' => 0,
      'project_release' => 0,
      'story' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_issues][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Issues',
      'weight' => '32',
      'description' => '',
      'type' => 'project_issue_nodereference_auto',
      'module' => 'project_issue',
    ),
  );

  // Exported field: field_tags
  $fields['initiative_issue_grouping-field_tags'] = array(
    'field_name' => 'field_tags',
    'type_name' => 'initiative_issue_grouping',
    'display_settings' => array(
      'weight' => '33',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'hidden',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_tags][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Tags',
      'weight' => '33',
      'description' => 'List of tags, separated by commas (for or) or + signs (for and)',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Issue groupings');
  t('Issues');
  t('Tags');

  return $fields;
}
