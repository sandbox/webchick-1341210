<?php

/**
 * Implementation of hook_views_default_views().
 */
function drupalorg_initiatives_views_default_views() {
  $views = array();

  // Exported view: drupalorg_initiatives_issues
  $view = new view;
  $view->name = 'drupalorg_initiatives_issues';
  $view->description = 'Issues for a given Drupal.org initiaitve grouping';
  $view->tag = 'Drupal.org';
  $view->base_table = 'node';
  $view->core = 6;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'assigned' => array(
      'label' => 'Assigned user',
      'required' => 1,
      'id' => 'assigned',
      'table' => 'project_issues',
      'field' => 'assigned',
      'relationship' => 'none',
    ),
    'rid' => array(
      'label' => 'Version',
      'required' => 0,
      'id' => 'rid',
      'table' => 'project_issues',
      'field' => 'rid',
      'relationship' => 'none',
    ),
    'pid' => array(
      'label' => 'Project node',
      'required' => 1,
      'id' => 'pid',
      'table' => 'project_issues',
      'field' => 'pid',
    ),
  ));
  $handler->override_option('fields', array(
    'project_issue_queue' => array(
      'label' => 'Project',
      'link_type' => 'issues',
      'exclude' => 0,
      'id' => 'project_issue_queue',
      'table' => 'node',
      'field' => 'project_issue_queue',
      'relationship' => 'pid',
    ),
    'title' => array(
      'label' => 'Summary',
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'timestamp' => array(
      'label' => '',
      'alter' => array(),
      'link_to_node' => 0,
      'comments' => 0,
      'exclude' => 0,
      'id' => 'timestamp',
      'table' => 'history_user',
      'field' => 'timestamp',
      'relationship' => 'none',
    ),
    'sid' => array(
      'id' => 'sid',
      'table' => 'project_issues',
      'field' => 'sid',
    ),
    'priority' => array(
      'id' => 'priority',
      'table' => 'project_issues',
      'field' => 'priority',
    ),
    'category' => array(
      'label' => 'Category',
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'category',
      'table' => 'project_issues',
      'field' => 'category',
      'relationship' => 'none',
    ),
    'comment_count' => array(
      'label' => 'Replies',
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'comment_count',
      'table' => 'node_comment_statistics',
      'field' => 'comment_count',
      'relationship' => 'none',
    ),
    'new_comments' => array(
      'label' => 'New replies',
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => ' new',
      'link_to_comment' => 1,
      'no_empty' => 1,
      'exclude' => 0,
      'id' => 'new_comments',
      'table' => 'node',
      'field' => 'new_comments',
      'relationship' => 'none',
    ),
    'last_comment_timestamp' => array(
      'label' => 'Last updated',
      'date_format' => 'raw time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'last_comment_timestamp',
      'table' => 'node_comment_statistics',
      'field' => 'last_comment_timestamp',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'Assigned to',
      'link_to_user' => 1,
      'overwrite_anonymous' => 1,
      'anonymous_text' => '',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'relationship' => 'assigned',
    ),
    'created' => array(
      'label' => 'Created',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'raw time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'score' => array(
      'label' => 'Score',
      'alter' => array(),
      'set_precision' => 1,
      'precision' => '3',
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'alternate_sort' => 'last_comment_timestamp',
      'alternate_order' => 'desc',
      'exclude' => 0,
      'id' => 'score',
      'table' => 'search_index',
      'field' => 'score',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'last_comment_timestamp' => array(
      'order' => 'DESC',
      'granularity' => 'second',
      'id' => 'last_comment_timestamp',
      'table' => 'node_comment_statistics',
      'field' => 'last_comment_timestamp',
      'relationship' => 'none',
    ),
    'score' => array(
      'order' => 'DESC',
      'id' => 'score',
      'table' => 'search_index',
      'field' => 'score',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action' => 'empty',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'node',
      'validate_fail' => 'not found',
      'break_phrase' => 1,
      'not' => 0,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        2 => 0,
        18 => 0,
        3 => 0,
        5 => 0,
        22 => 0,
        20 => 0,
        24 => 0,
        12 => 0,
        16 => 0,
        4 => 0,
        14 => 0,
        7 => 0,
        8 => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'project_issue' => 'project_issue',
        'initiative' => 0,
        'initiative_issue_grouping' => 0,
        'forum' => 0,
        'project_project' => 0,
        'project_release' => 0,
        'book' => 0,
        'page' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nids',
      'validate_argument_vocabulary' => array(
        1 => 0,
        5 => 0,
        3 => 0,
        2 => 0,
        4 => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_project_term_argument_type' => 'tid',
      'validate_argument_project_term_argument_action_top_without' => 'pass',
      'validate_argument_project_term_argument_action_top_with' => 'pass',
      'validate_argument_project_term_argument_action_child' => 'pass',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'status_extra' => array(
      'operator' => '=',
      'value' => '',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status_extra',
      'table' => 'node',
      'field' => 'status_extra',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Issues for all projects');
  $handler->override_option('empty', 'No issues match your criteria.');
  $handler->override_option('empty_format', '1');
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'project_issue_table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'columns' => array(
      'project_issue_queue' => 'project_issue_queue',
      'title' => 'title',
      'timestamp' => 'title',
      'sid' => 'sid',
      'priority' => 'priority',
      'category' => 'category',
      'comment_count' => 'comment_count',
      'new_comments' => 'comment_count',
      'last_comment_timestamp' => 'last_comment_timestamp',
      'name' => 'name',
      'created' => 'created',
      'score' => 'score',
    ),
    'info' => array(
      'project_issue_queue' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'title' => array(
        'sortable' => 1,
        'separator' => ' ',
      ),
      'timestamp' => array(
        'separator' => '',
      ),
      'sid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'priority' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'category' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'comment_count' => array(
        'sortable' => 1,
        'separator' => '<br />',
      ),
      'new_comments' => array(
        'separator' => '',
      ),
      'last_comment_timestamp' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'score' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'score',
  ));

  $views[$view->name] = $view;

  // Exported view: drupalorg_initiatives_issues_tag
  $view = new view;
  $view->name = 'drupalorg_initiatives_issues_tag';
  $view->description = 'Issues for a given Drupal.org initiaitve grouping (by tag)';
  $view->tag = 'Drupal.org';
  $view->base_table = 'node';
  $view->core = 6;
  $view->api_version = '2';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'assigned' => array(
      'label' => 'Assigned user',
      'required' => 1,
      'id' => 'assigned',
      'table' => 'project_issues',
      'field' => 'assigned',
      'relationship' => 'none',
    ),
    'rid' => array(
      'label' => 'Version',
      'required' => 0,
      'id' => 'rid',
      'table' => 'project_issues',
      'field' => 'rid',
      'relationship' => 'none',
    ),
    'pid' => array(
      'label' => 'Project node',
      'required' => 1,
      'id' => 'pid',
      'table' => 'project_issues',
      'field' => 'pid',
    ),
  ));
  $handler->override_option('fields', array(
    'project_issue_queue' => array(
      'label' => 'Project',
      'link_type' => 'issues',
      'exclude' => 0,
      'id' => 'project_issue_queue',
      'table' => 'node',
      'field' => 'project_issue_queue',
      'relationship' => 'pid',
    ),
    'title' => array(
      'label' => 'Summary',
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'timestamp' => array(
      'label' => '',
      'alter' => array(),
      'link_to_node' => 0,
      'comments' => 0,
      'exclude' => 0,
      'id' => 'timestamp',
      'table' => 'history_user',
      'field' => 'timestamp',
      'relationship' => 'none',
    ),
    'sid' => array(
      'id' => 'sid',
      'table' => 'project_issues',
      'field' => 'sid',
    ),
    'priority' => array(
      'id' => 'priority',
      'table' => 'project_issues',
      'field' => 'priority',
    ),
    'category' => array(
      'label' => 'Category',
      'link_to_node' => 0,
      'exclude' => 0,
      'id' => 'category',
      'table' => 'project_issues',
      'field' => 'category',
      'relationship' => 'none',
    ),
    'comment_count' => array(
      'label' => 'Replies',
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'exclude' => 0,
      'id' => 'comment_count',
      'table' => 'node_comment_statistics',
      'field' => 'comment_count',
      'relationship' => 'none',
    ),
    'new_comments' => array(
      'label' => 'New replies',
      'set_precision' => FALSE,
      'precision' => 0,
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => ' new',
      'link_to_comment' => 1,
      'no_empty' => 1,
      'exclude' => 0,
      'id' => 'new_comments',
      'table' => 'node',
      'field' => 'new_comments',
      'relationship' => 'none',
    ),
    'last_comment_timestamp' => array(
      'label' => 'Last updated',
      'date_format' => 'raw time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'last_comment_timestamp',
      'table' => 'node_comment_statistics',
      'field' => 'last_comment_timestamp',
      'relationship' => 'none',
    ),
    'name' => array(
      'label' => 'Assigned to',
      'link_to_user' => 1,
      'overwrite_anonymous' => 1,
      'anonymous_text' => '',
      'exclude' => 0,
      'id' => 'name',
      'table' => 'users',
      'field' => 'name',
      'relationship' => 'assigned',
    ),
    'created' => array(
      'label' => 'Created',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'date_format' => 'raw time ago',
      'custom_date_format' => '',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'score' => array(
      'label' => 'Score',
      'alter' => array(),
      'set_precision' => 1,
      'precision' => '3',
      'decimal' => '.',
      'separator' => ',',
      'prefix' => '',
      'suffix' => '',
      'alternate_sort' => 'last_comment_timestamp',
      'alternate_order' => 'desc',
      'exclude' => 0,
      'id' => 'score',
      'table' => 'search_index',
      'field' => 'score',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'last_comment_timestamp' => array(
      'order' => 'DESC',
      'granularity' => 'second',
      'id' => 'last_comment_timestamp',
      'table' => 'node_comment_statistics',
      'field' => 'last_comment_timestamp',
      'relationship' => 'none',
    ),
    'score' => array(
      'order' => 'DESC',
      'id' => 'score',
      'table' => 'search_index',
      'field' => 'score',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'name' => array(
      'default_action' => 'empty',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'taxonomy_term',
      'validate_fail' => 'empty',
      'glossary' => 0,
      'ignorecase' => 0,
      'limit' => '0',
      'case' => 'none',
      'path_case' => 'none',
      'transform_dash' => 0,
      'add_table' => 1,
      'require_value' => 0,
      'id' => 'name',
      'table' => 'term_data',
      'field' => 'name',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        2 => 0,
        18 => 0,
        3 => 0,
        5 => 0,
        22 => 0,
        20 => 0,
        24 => 0,
        12 => 0,
        16 => 0,
        4 => 0,
        14 => 0,
        7 => 0,
        8 => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
        'initiative' => 0,
        'initiative_issue_grouping' => 0,
        'forum' => 0,
        'project_project' => 0,
        'project_release' => 0,
        'project_issue' => 0,
        'book' => 0,
        'page' => 0,
        'story' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        5 => 5,
        1 => 0,
        3 => 0,
        2 => 0,
        4 => 0,
      ),
      'validate_argument_type' => 'name',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_project_term_argument_type' => 'tid',
      'validate_argument_project_term_argument_action_top_without' => 'pass',
      'validate_argument_project_term_argument_action_top_with' => 'pass',
      'validate_argument_project_term_argument_action_child' => 'pass',
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'status_extra' => array(
      'operator' => '=',
      'value' => '',
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'status_extra',
      'table' => 'node',
      'field' => 'status_extra',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Issues for all projects');
  $handler->override_option('empty', 'No issues match your criteria.');
  $handler->override_option('empty_format', '1');
  $handler->override_option('items_per_page', 50);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'project_issue_table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'desc',
    'columns' => array(
      'project_issue_queue' => 'project_issue_queue',
      'title' => 'title',
      'timestamp' => 'title',
      'sid' => 'sid',
      'priority' => 'priority',
      'category' => 'category',
      'comment_count' => 'comment_count',
      'new_comments' => 'comment_count',
      'last_comment_timestamp' => 'last_comment_timestamp',
      'name' => 'name',
      'created' => 'created',
      'score' => 'score',
    ),
    'info' => array(
      'project_issue_queue' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'title' => array(
        'sortable' => 1,
        'separator' => ' ',
      ),
      'timestamp' => array(
        'separator' => '',
      ),
      'sid' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'priority' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'category' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'comment_count' => array(
        'sortable' => 1,
        'separator' => '<br />',
      ),
      'new_comments' => array(
        'separator' => '',
      ),
      'last_comment_timestamp' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'name' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'created' => array(
        'sortable' => 1,
        'separator' => '',
      ),
      'score' => array(
        'sortable' => 1,
        'separator' => '',
      ),
    ),
    'default' => 'score',
  ));

  $views[$view->name] = $view;

  return $views;
}
